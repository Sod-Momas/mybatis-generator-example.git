# mybatis-generator-example

该项目演示使用 mybatis-generator 从数据库生成工具类

> 关联文章 : [MyBatis Generator 生成器把其他数据库的同名表生成下来的问题](https://blog.csdn.net/qq_40233736/article/details/83314596)

## 使用方法

1. 修改 `src\test\resources\generatorConfig.xml` 的 `jdbcConnection` 节点, 把自己的数据库用户名填进去
2. 添加 `src\test\resources\generatorConfig.xml` 的 `table` 节点, 把需要生成的表名填进去
3. 运行 `cc.momas.mgb.MybatisGeneratorExampleApplicationTests` 
4. 控制台打印以下日志表示完成, 如果有其他警告信息,请按照提示内容搜索

```log
17:00:50.383 [main] DEBUG org.mybatis.generator.logging.LogFactory - Logging initialized using 'org.mybatis.generator.logging.slf4j.Slf4jLoggingLogFactory@2dc54ad4' adapter.
17:00:50.389 [main] DEBUG org.mybatis.generator.internal.db.DatabaseIntrospector - Retrieving column information for table "user"
17:00:50.426 [main] DEBUG org.mybatis.generator.internal.db.DatabaseIntrospector - Found column "id", data type 1, in table "test..user"
17:00:50.426 [main] DEBUG org.mybatis.generator.internal.db.DatabaseIntrospector - Found column "username", data type 12, in table "test..user"
17:00:50.426 [main] DEBUG org.mybatis.generator.internal.db.DatabaseIntrospector - Found column "password_", data type 1, in table "test..user"
17:00:50.426 [main] DEBUG org.mybatis.generator.internal.db.DatabaseIntrospector - Found column "password_salt", data type 1, in table "test..user"
17:00:50.426 [main] DEBUG org.mybatis.generator.internal.db.DatabaseIntrospector - Found column "email", data type 1, in table "test..user"
17:00:50.426 [main] DEBUG org.mybatis.generator.internal.db.DatabaseIntrospector - Found column "display_name", data type 12, in table "test..user"
=================================
[]
=================================
```