CREATE TABLE test.user
(
    id            CHAR(36)                NOT NULL COMMENT '用户ID' PRIMARY KEY,
    username      VARCHAR(128)            NOT NULL COMMENT '用户名,用于登录',
    password_     CHAR(64)                NOT NULL COMMENT '用户密码',
    password_salt CHAR(64)                NULL COMMENT '密码盐,用于密码加密',
    email         CHAR(64)                NULL COMMENT '电子邮箱',
    display_name  VARCHAR(128) DEFAULT '' NULL COMMENT '用户昵称'
)
    COMMENT '用户表' COLLATE = UTF8MB4_BIN;


insert into `user`(id, username, password_, password_salt, email) values (uuid(),'sod',md5('sod123'),'123','user@domain.com');