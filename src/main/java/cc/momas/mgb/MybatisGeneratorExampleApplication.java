package cc.momas.mgb;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisGeneratorExampleApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(MybatisGeneratorExampleApplication.class, args);
    }

    @Override
    public void run(String... args) {
        System.out.println("Hello World");
    }
}
