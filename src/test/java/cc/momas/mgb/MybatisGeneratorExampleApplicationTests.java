package cc.momas.mgb;

import org.junit.Test;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.exception.InvalidConfigurationException;
import org.mybatis.generator.exception.XMLParserException;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class MybatisGeneratorExampleApplicationTests {

    @Test
    public void contextLoads() throws IOException, XMLParserException, InvalidConfigurationException, SQLException, InterruptedException {
        // 警告消息列表
        List<String> warnings = new ArrayList<>();
        // 读取classpath:下的配置文件
        InputStream configFile = MybatisGeneratorExampleApplicationTests.class.getClassLoader().getResourceAsStream("generatorConfig.xml");
        // 读取绝对路径
		// File configFile = new File("generatorConfig.xml");
        // 使用 Spring 工具类读取 classpath下的配置文件
        // ClassPathResource configFile = new ClassPathResource("generatorConfig.xml");

        // 将配置文件转为java类
        ConfigurationParser cp = new ConfigurationParser(warnings);
        Configuration config = cp.parseConfiguration(configFile);
        // 配置回调, true表示会覆盖原有的文件
        DefaultShellCallback callback = new DefaultShellCallback(true);
        // 创建生成器实例
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
        // 开始生成
        myBatisGenerator.generate(null);
        // 转换完成
        System.out.println("=================================");
        System.out.println(warnings);
        System.out.println("=================================");
    }

}
